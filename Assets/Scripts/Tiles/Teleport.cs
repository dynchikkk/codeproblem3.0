using System.Collections;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    [Header("Base")]
    [SerializeField] private Teleport _endTeleport;
    [SerializeField] private float _teleportCoolDown = 2;

    private bool _canTeleport;
    public bool CanTeleport
    {
        get => _canTeleport;
        set
        {
            if (value is false)
            {
                SetMaterial(_closedMaterial);
            }

            if (value is true)
            {
                SetMaterial(_openMaterial);
            }

            _canTeleport = value;
        }
    }

    [Header("Materials")]
    [SerializeField] private Material _closedMaterial;
    [SerializeField] private Material _openMaterial;

    private MeshRenderer _renderer;

    private void Awake()
    {
        _renderer = GetComponent<MeshRenderer>();
        CanTeleport = true;
    }

    private void OnTriggerStay(Collider other)
    {
        TeleportObject(other.gameObject);
    }

    public IEnumerator ResetTeleport()
    {
        yield return new WaitForSeconds(_teleportCoolDown);
        CanTeleport = true;
    }

    private void TeleportObject(GameObject other)
    {
        if (CanTeleport is false)
            return;

        CanTeleport = false;
        _endTeleport.CanTeleport = false;

        other.transform.position = _endTeleport.transform.position;

        StartCoroutine(nameof(ResetTeleport));
        _endTeleport.StartCoroutine(nameof(ResetTeleport));
    }

    public void SetMaterial(Material toSet)
    {
        _renderer.sharedMaterial = toSet;
    }
}
