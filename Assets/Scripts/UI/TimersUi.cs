using UnityEngine;
using TMPro;

public class TimersUi : MonoBehaviour
{
    [SerializeField] private TMP_Text _timerText;
    [SerializeField] private TMP_Text _bestTimeText;
    private Game _localGame;

    private void Awake()
    {
        _localGame = Game.Instance;
    }

    private void OnEnable()
    {
        _localGame.OnBestTimeChange += SetBestTimeText;
        _localGame.OnTimerChange += SetTimerText;
    }

    private void OnDisable()
    {
        _localGame.OnBestTimeChange -= SetBestTimeText;
        _localGame.OnTimerChange -= SetTimerText;
    }

    private void SetBestTimeText(float bestTime)
    {
        if (_bestTimeText is null)
            return;

        _bestTimeText.text = string.Format("{0:0.00}", bestTime);
    }

    private void SetTimerText(float timer)
    {
        if (_timerText is null)
            return;

        _timerText.text = string.Format("{0:0.00}", timer);
    }
}
