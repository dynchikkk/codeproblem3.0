﻿using UnityEngine;
using System;

[RequireComponent(typeof(SceneChanger))]
public class Game : MonoBehaviour
{
    public static Game Instance;

    // Events
    // Time
    public event Action<float> OnTimerChange;
    public event Action<float> OnBestTimeChange;
    // Win Loose 
    public event Action OnWinEvent;
    public event Action OnLooseEvent;

    [Header("Timer")]
    [SerializeField] private bool _timerIsOn;
    [SerializeField] private float _timerValue;

    [Header("Objects")]
    [SerializeField] private Player _player;
    [SerializeField] private Exit _exitFromLevel;
    [SerializeField] private GameObject _keyParent;

    private int _keyGloal;
    private float _timer = 0;
    private bool _gameIsEnded = false;
    private SceneChanger sceneChanger;

    private void Awake()
    {
        Instance = this;
        sceneChanger = GetComponent<SceneChanger>();
        _timer = _timerValue;
        _keyGloal = _keyParent.transform.childCount;
    }

    private void Start()
    {
        _exitFromLevel.Close();

        OnBestTimeChange?.Invoke(GameData.bestTimes[sceneChanger.GetCurrentScene()]);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            sceneChanger.ChangeScene(sceneChanger.GetCurrentSceneString());
        }

        if(_gameIsEnded)
            return;
        
        TimerTick();
        LookAtPlayerHealth();
        LookAtPlayerInventory();
    }

    #region TimeWork
    private void TimerTick()
    {
        if(_timerIsOn == false)
            return;
        
        _timer -= Time.deltaTime;
        OnTimerChange?.Invoke(_timer);

        if (_timer <= 0)
            Lose();
    }

    private void CheckBestTime()
    {
        float bestTime = GameData.bestTimes[sceneChanger.GetCurrentScene()];
        float currentTime = _timerValue - _timer;

        if (bestTime > currentTime)
        {
            GameData.bestTimes[sceneChanger.GetCurrentScene() ] = currentTime;
        }
        OnBestTimeChange?.Invoke(currentTime);
    }
    #endregion

    #region WinLoose
    private void Victory()
    {
        _gameIsEnded = true;
        CheckBestTime();
        OnWinEvent?.Invoke();
        _player.Disable();
        Debug.LogError("Victory");
    }

    private void Lose()
    {
        _gameIsEnded = true;
        OnLooseEvent?.Invoke();
        _player.Disable();
        Debug.LogError("Lose");
    }
    #endregion

    public void TryCompleteLevel()
    {
        if (_exitFromLevel.IsOpen is false)
            return;

        Victory();
    }

    private void LookAtPlayerHealth()
    {
        if (_player.IsAlive)
            return;

        Lose();
        Destroy(_player.gameObject);
    }

    private void LookAtPlayerInventory()
    {
        if (_player.KeyCount >= _keyGloal)
            _exitFromLevel.Open();
    }
}
