using UnityEngine;
using System.Collections;

public class FireEnemy : Enemy
{
    [Header("Bullets attribute")]
    [SerializeField] protected Bullet _bullet;
    [SerializeField] protected GameObject _bulletPlace;

    protected Pooler<Bullet> _bulletPool;

    protected void Awake()
    {
        const int PreStartCount = 5;
        _bulletPool = new Pooler<Bullet>(_bullet, PreStartCount, _bulletPlace.transform);
    }

    protected void OnEnable()
    {
        StartCoroutine(nameof(Shot));
    }

    protected void OnDisable()
    {
        StopAllCoroutines();
    }

    protected IEnumerator Shot()
    {
        while (true)
        {
            Fire();
            yield return new WaitForSeconds(_patrolDelay);
        }
    }

    protected void Fire()
    {
        var bullet = _bulletPool.GetFreeElement();
        bullet.transform.position = _bulletPlace.transform.position;
        bullet.transform.rotation = _bulletPlace.transform.rotation;
    }
}
