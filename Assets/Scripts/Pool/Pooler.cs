using System.Collections.Generic;
using UnityEngine;

public class Pooler<T> where T : MonoBehaviour
{
    public T prefab;
    public bool autoExpand;
    public Transform container;

    private List<T> pool;

    public Pooler(T prefab, int count, bool autoExpand = true)
    {
        this.prefab = prefab;
        this.container = null;
        this.autoExpand = autoExpand;

        this.CreatePool(count);
    }

    public Pooler(T prefab, int count, Transform container, bool autoExpand = true)
    {
        this.prefab = prefab;
        this.autoExpand = autoExpand;
        this.container = container;

        this.CreatePool(count);
    }

    private void CreatePool (int count)
    {
        this.pool = new List<T>();

        for (int i = 0; i < count; i++)
        {
            this.CreateObject();
        }
    }

    private T CreateObject (bool isActiveByDefault = false)
    {
        var createdObject = Object.Instantiate(this.prefab, this.container);
        createdObject.gameObject.SetActive(isActiveByDefault);
        createdObject.name += createdObject.GetInstanceID().ToString();

        this.pool.Add(createdObject);
        return createdObject;
    }

    public bool HasFreeElement(out T element)
    {
        foreach (var mono in pool)
        {
            if (!mono.gameObject.activeInHierarchy)
            {
                element = mono;
                mono.gameObject.SetActive(true);
                return true;
            }
        }

        element = null;
        return false;
    }

    public T GetFreeElement()
    {
        if (this.HasFreeElement(out var element))
            return element;

        if (this.autoExpand)
            return this.CreateObject(true);


        throw new System.Exception($"No free element in pool of type {typeof(T)}");
    }
}
