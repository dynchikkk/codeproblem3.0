using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] protected float _patrolDelay;

    protected void OnTriggerEnter(Collider other)
    {
        DoingWithOtherOnTriggerEnter(other);
    }

    protected virtual void DoingWithOtherOnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player))
            player.Kill();
    }
}
