﻿using UnityEngine;
using System.Collections;

public class PointsObserver : MonoBehaviour
{
    [Header("Observable")]
    [SerializeField] private Transform[] _observablePoints;
    [SerializeField] private float _lookDelay;
    
    [Header("Field of View")]
    [SerializeField] private GameObject _fieldOfView;
    [SerializeField] private LayerMask _obstacleMask;
    [SerializeField] private LayerMask _playerMask;

    private float _timer = 0f;
    private int _currentObservablePointIndex = 0;

    private void Awake()
    {
        _timer = _lookDelay;
    }

    private void OnEnable()
    {
        StartCoroutine(nameof(LookAtTimerTick));
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private IEnumerator LookAtTimerTick()
    {
        while (true)
        {
            int nextPointIndex = _currentObservablePointIndex + 1;

            if (nextPointIndex >= _observablePoints.Length)
                nextPointIndex = 0;

            _currentObservablePointIndex = nextPointIndex;

            LookAtPoint(_currentObservablePointIndex);

            yield return new WaitForSeconds(_lookDelay);
        }
    }

    private void LookAtPoint(int pointIndex)
    {
        Vector3 distanceToPoint = _observablePoints[pointIndex].transform.position - transform.position;
        Vector3 directionToPoint = distanceToPoint.normalized;

        if (Physics.Raycast(transform.position, directionToPoint, out RaycastHit hit, distanceToPoint.magnitude, _playerMask))
            hit.collider.GetComponent<Player>().Kill();

        _fieldOfView.SetActive(Physics.Raycast(transform.position, directionToPoint, distanceToPoint.magnitude, _obstacleMask) == false);
        transform.forward = new Vector3(directionToPoint.x, 0f, directionToPoint.z);
    }
}
