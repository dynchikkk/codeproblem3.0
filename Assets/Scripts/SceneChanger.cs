using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneChanger : MonoBehaviour
{
    public void ChangeScene(string _name)
    {
        SceneManager.LoadScene(_name);
    }

    public int GetCurrentScene()
    {
        return SceneManager.GetActiveScene().buildIndex - 1;
    }

    public string GetCurrentSceneString()
    {
        return SceneManager.GetActiveScene().name;
    }
}
