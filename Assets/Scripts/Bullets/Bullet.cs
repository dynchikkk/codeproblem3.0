using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour, IPoolObject
{
    [SerializeField] protected float _bulletSpeed = 2;

    protected Rigidbody _rigidbody;

    protected void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    protected void FixedUpdate()
    {
        Fly();
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            player.Kill();
        }

        if (other.gameObject.layer == 8 || other.gameObject.layer == 9)
            ResetBeforeBackToPool();
    }

    protected virtual void Fly()
    {
        _rigidbody.velocity = transform.forward * _bulletSpeed;
    }

    public void ResetBeforeBackToPool()
    {
        gameObject.SetActive(false);
    }
}
