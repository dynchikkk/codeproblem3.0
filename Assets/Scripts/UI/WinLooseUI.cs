using TMPro;
using UnityEngine;

public class WinLooseUI : MonoBehaviour
{
    public TMP_Text showedText;
    private Game _localGame;

    private void Awake()
    {
        _localGame = Game.Instance;
    }

    private void OnEnable()
    {
        _localGame.OnLooseEvent += Lose;
        _localGame.OnWinEvent += Win;
    }

    private void OnDisable()
    {
        _localGame.OnLooseEvent -= Lose;
        _localGame.OnWinEvent -= Win;
    }

    private void Start()
    {
        showedText.gameObject.SetActive(false);
    }

    private void Lose()
    {
        showedText.gameObject.SetActive(true);
        showedText.text = "Lose";
        showedText.color = Color.red;
    }

    private void Win()
    {
        showedText.gameObject.SetActive(true);
        showedText.text = "Win";
        showedText.color = Color.green;
    }
}
