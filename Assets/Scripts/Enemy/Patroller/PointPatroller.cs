﻿using UnityEngine;
using System.Collections;

public class PointPatroller : Enemy
{
    [SerializeField] private Transform[] _patrollingPoints;
    [SerializeField] private float _step;

    private int _currentPatrolPointIndex = 0;

    private void OnEnable()
    {
        StartCoroutine(nameof(PatrolTimerTick));
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private IEnumerator PatrolTimerTick()
    {
        while (true)
        {
            int nextPointIndex = _currentPatrolPointIndex + 1;

            if (nextPointIndex >= _patrollingPoints.Length)
                nextPointIndex = 0;

            _currentPatrolPointIndex = nextPointIndex;

            MoveToPoint(_currentPatrolPointIndex);

            yield return new WaitForSeconds(_patrolDelay);
        }
    }

    private void MoveToPoint(int pointIndex)
    {
        transform.position = new Vector3(_patrollingPoints[pointIndex].position.x, transform.position.y, _patrollingPoints[pointIndex].position.z);
    }
}
